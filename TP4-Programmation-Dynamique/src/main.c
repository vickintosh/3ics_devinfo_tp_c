/**
 * @file main.c
 * @author Victor Minon
 * @version 1.0
 * @brief main page of ProgDyn project
 * @date 2021-10-20
 * 
 * @copyright Copyright (c) 2021
 */


/*! \mainpage ProgDyn
 *
 *This project start 3 functions :
 *
 * \subsection function1 Find by dichotomy
 * This function search a value in a list and return the index of his value.
 * \n This function use the dichotomy method that means, the algorithm halves the search space until finding the value. 
 * \n Divide and conquer.
 * 
 * \subsection function2 Knapsack problem
 * This function resolve the knapsac problem.
 * \n The algorithm consists of putting the most object in a backpack.
 * \n Of cours the backpack has a maximum weight and object also have a weight  
 * 
 * \subsection function3 Problem of the largest free square
 
 */



#include <stdio.h>
#include "../include/dychotomy.h"
#include "../include/glouton.h"
#include "../include/pgcb.h"



int main()
{
	//test dichotomy
	int t[]={0,1,2,5,6,7,8,45,65,325,456,1254};
	int index;
	int size=12;
	int searchedValue=325;

	index=find_by_dichotomy(t,size,searchedValue);

	printf("L'index de %d dans le tableau est : %d\n",searchedValue,index);
	//printf("%d",t[9]);



	//test knapsack

	Object o1, o2, o3;
	o1.weight = 6;o1.cost = 7;
	o2.weight = 5;o2.cost = 5;
	o3.weight = 5;o3.cost = 5;

	Object item[3] = {o1, o2,o3};
	Object *backpack;
	backpack = knapsack(item, 3, 10);

	/**
	 *the algorithm is not optimal for the previous set of values,
	 *because it adds the objects in the backpack according to their cost/weight ratio in a decreasing way
	 */

	

	return (0);
}